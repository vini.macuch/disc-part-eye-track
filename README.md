# Discourse Particle Processing

This repository contains the files pertaining to the **Discourse Particle Processing** (DPP) study, which encompasses several daughter experiments, both internet- and laboratory-based. The repository contains all of the materials (pictures, audio files, and trial information) needed to realize experiments in the folder `materials`. Inside the folder `materials` the single most important document is `disc-part-items.csv`, which we also refer to as **master CSV** because it is an exhaustive data table listing all the items available for usage in the various experiments related to the DPP.

The folder `experiments` contains information about the various experiments which are based on this material.

More information can be found in the `README.md` files in each relevant subfolder.

