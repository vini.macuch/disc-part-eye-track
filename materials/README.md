# Materials for DPP

This folder contains the material for the realization of DPP experiments (mouse-, eye-tracking, and behavioral studies). There are three main folders:

+ `pictorial-stimuli`: contains all pictures for use in experiments
+ `processed_pics`: contains all pictures, rescaled to a sensible size for use in the experiments
+ `audio-stimuli`: contains all audio files for use in experiments
+ `Audio`: contains the raw audio sources (not useful for the experiments!)
+ `audio_durations.csv` contains the filenames & durations of the files in `audio-stimuli`

There is also the **master CSV** file which is `disc-part-items.csv`. (There is also a version in a different encoding scheme, which is a relic of developing this on a different computer. It is here for internal backward compatibility, but should/might not be used anymore.)

## Content of the master CSV (long version)

The basic structure of the *master CSV* is the following: each row consists of one experimental trial, to be instantiated, for example, in a two-alternative forced choice task, or a in language comprehension experiment, such as a visual world eye-tracking task. The key aspect of the data is that the trials are built around a quadruplet of visual stimuli, namely two pictures of either objects or animals, which have been paired, by design, on the basis of a salient semantic and/ or visual similarity, together with two dissimilar distractor pictures.

Most variables (encoded in columns) are relevant primarily in the context of a referential decision task, such that the experimental trials have the following structure: each trial, a participant is presented with a visual display as well as with two linguistic stimuli, namely a question and an answer. The question directly refers to one of the pictures in the target picture pair, while the answer, when presented in its entirety, directly refers to either the mentioned picture or the competing picture in the pair.

The most relevant variables in the item inventory which correspond to each element in this trial structure are: 

*  **mentioned_element**: the target pair element mentioned in the question stimulus -- variable contains picture name (in English);
* **unmentioned_element**: the target pair element not mentioned in the question stimulus, i.e. the competitor -- variable contains picture name (in English);
*  **target**: the target pair element mentioned in the question stimulus -- variable contains object or animal name as it appears in the answer stimulus (same name as in question stimulus, in German);
*  **competitor**: the target pair element not mentioned in the question stimulus -- variable contains object or animal name as it appears in the answer stimulus (in German);
*  **expected_response**: the correct response acccording to the experimental design -- variable contains object or animal name as it appears in the answer stimulus (in German).

In addition to the critical items described above, the item inventory contains filler items. Unlike critical items, fillers are built not on the basis of target picture pairs but rather on the basis of one target picture and one semantically and/ or visually dissimilar picture. Since filler trials contain only one relevant picture, the variables **unmentioned_element** and **competitor** contain empty entries. 
The object or animal mentioned in the question stimulus is encoded in the variable **expected_response**, which, unlike critical items, contains the name of the corresponding target picture (in English).

As it stands, the item inventory can be used to generate trials with either a 2- or a 4-picture visual display. In a 4-picture display, the basic trial structure is extended by introducing 2 pictures which are dissimilar to those in the basic pair (or, in the case of fillers, dissimilar to the target picture). These have been designed to be used as distractors.

## Content of the master CSV (in a nutshel)

- the master CSV is in file `disc-part-items.csv`
- contains all info for all trials necessary for different realization of different experiments
- contains 260 rows, of which 200 critical and 60 filler trials (column `item_type`)
- within the 200 critical trials there are 120 reliable and 80 unreliable trials (column `condition`)
- within the 120 critical trials, half are for 'eigentlich', the other half for 'tatsächlich' (column `DP` (= 'discourse particle'))
- there are 160 different quadruplets of pictures (e.g., items) (column `item_name`)
  - each item occurs either as 
    1. as reliable trial for 'eigentlich' and as reliable trial for 'tatsächlich', or
    2. as reliable trial for 'eigentlich' and as reliable trial for 'tatsächlich', or 
    3. as filler

