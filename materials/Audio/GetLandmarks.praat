form Intensity parameters:
	comment Leave the directory path empty if you want to use the current directory.
	sentence directory /Users/timoroettger/repos/disc-part-eye-track/Audio/Original/Answers/
	comment Which tier should be the landmark tier?
	integer tiert 3
endform

result_file$ = directory$+"result.txt"
filedelete 'result_file$'

Create Strings as file list...  file_list 'directory$'*.TextGrid
number_of_files = Get number of strings
nr_completed = 0
fileappend result.txt file 'tab$'stimulus'tab$'onset'tab$'offset'tab$'adverb_onset'tab$'referent_onset'newline$'

for i_file to number_of_files

	select Strings file_list
	grid_file_name$ = Get string... i_file
	Read from file... 'directory$''grid_file_name$'
	base_name$ = selected$("TextGrid") 
	select TextGrid 'base_name$'
	fileappend result.txt 'grid_file_name$' 

		numberOfItems = Get number of intervals... 1
		numberOflandmarks = Get number of intervals... 2
			
			for i to numberOfItems
				label$ = Get label of interval... 1 i
				if label$ <> ""
					fileappend result.txt 'tab$' 'label$'
					onset = Get start point... 1 i
					offset = Get end point... 1 i		
					fileappend result.txt 'tab$' 'onset:3' 'tab$' 'offset:3'
						for j from 1 to numberOflandmarks
							label2$ = Get label of interval... 2 j
							adverb = Get start point... 2 j
							referent = Get end point... 2 j
							if adverb > onset and referent < offset and label2$ <> ""
								fileappend result.txt 'tab$' 'adverb:3' 'tab$' 'referent:3'
							endif
						endfor
				endif
				fileappend result.txt 'tab$' 'newline$'
			endfor										
endfor			 

# Object window clean-up 
select all
Remove