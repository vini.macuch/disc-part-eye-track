# This script outputs training trials (10, "reliable"),
# which are identical for each subject,
# and will be randomised in OpSe

library(tidyverse)

generate_training_CSV = function(participant_number, group_type) {

  # Load item inventory from master CSV file
  # assuming that the cwd is the root of the repo
  d <- read_csv("materials/disc-part-items.csv")

  # Add some variables specific to the mousetracking experiment in OpSe
  d$MT_target <- d$picture_target
  d$MT_pic_left <- d$picture_target
  d$MT_pic_right <- d$picture_competitor

  # number of blocks
  n_blocks = 1

  # determine how many trials of each type to extract for each block
  # this depends on the group type ("reliable" vs "unreliable")
  # NB: all numbers supplied here must be multiples of `n_blocks`!
    n_reliable_actually   = 2 * n_blocks
    n_reliable_indeed     = 2 * n_blocks
    n_unreliable_actually = 0 * n_blocks
    n_unreliable_indeed   = 0 * n_blocks
    n_fillers             = 6 * n_blocks

  # draw `n` trials of condition `condition` and with DP `DP`
  # w/o replacement from master CSV
  # avoid any item supplied in `ignore_items`
  plug_trials = function(DP_select, condition_select, n_samples, ignore_items = "") {
    # show(ignore_items)
    d %>%
      filter(
        DP == DP_select,
        condition == condition_select,
        ! item_name %in% ignore_items
      ) %>%
      sample_n(n_samples, replace = F) %>%
      # add a block number 1, 2, ..., n_blocks
      mutate(block = rep(1:n_blocks, each = n_samples / n_blocks))
    # show(str_c("intersection: ", intersect(out$item_name, ignore_items)))
  }

  # NB: items for reliable and unreliable trials are disjoint already in master CSV
  reliable_actually   = plug_trials("actually", "reliable",   n_reliable_actually)
  reliable_indeed     = plug_trials("indeed",   "reliable",   n_reliable_indeed,    reliable_actually$item_name)
  unreliable_actually = plug_trials("actually", "unreliable", n_unreliable_actually)
  unreliable_indeed   = plug_trials("indeed",   "unreliable", n_unreliable_indeed,  unreliable_actually$item_name)

  # select `n_fillers` filler trials at random w/o replacement
  fillers = d %>%
    filter(item_type == "filler") %>%
    sample_n(n_fillers, replace = F) %>%
    # add a list number 1, 2, ..., n_blocks
    mutate(block = rep(1:n_blocks, each = n_fillers / n_blocks))

  # join all tibbles in a list
  selected_trials <-  list(
    reliable_actually,
    reliable_indeed,
    unreliable_actually,
    unreliable_indeed,
    fillers
  ) %>%
    # discard empty tibbles
    discard(function(x) {nrow(x) == 0}) %>%
    # reduce list -> append each DF to previous
    reduce(rbind) %>%
    # shuffle the trials in each block (randomly)
    group_by(block) %>%
    mutate(
      trial_nr_in_block = sample(1:10, size = 10, replace = F),
      group_type = group_type
    ) %>%
    # order by block and, within each block, by trial_nr_in_block
    arrange(block, trial_nr_in_block)

  # File names start with "!_" to get them to appear at the start of OpenSesame's file pool
  write_csv(selected_trials, "experiments/mouse-tracking/trial-data/!_training.csv")

  selected_trials

}

generate_training_CSV("00", "reliable")
