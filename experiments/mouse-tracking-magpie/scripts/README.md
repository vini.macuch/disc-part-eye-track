# structure of master data 
- the master CSV is in file `disc-part-items.csv`
- contains all info for all trials necessary for different realization of different experiments
- contains 260 rows, of which 200 critical and 60 filler trials (column `item_type`)
- within the 200 critical trials there are 120 reliable and 80 unreliable trials (column `condition`)
- within the 120 critical trials, half are for 'eigentlich', the other half for 'tatsächlich' (column `DP` (= 'discourse particle'))
- there are 160 different quadruplets of pictures (e.g., items) (column `item_name`)
- each item occurs either 
    1. as reliable trial for 'eigentlich' and as reliable trial for 'tatsächlich', or
    2. as reliable trial for 'eigentlich' and as reliable trial for 'tatsächlich', or 
    3. as filler
    
# caveat
- remove 'cigar' item altogether b/c it has no sound file (after creating a list)

# procedure (main experiment part)
- each participant is initially allocated to either the reliable or the unreliable group
- each participant sees 10 training trials (in a sequence randomized per subject); these are all 'reliable', 2 'actually', 2 'indeed', 6 filler
  + it is possible that training items are repeated in the main part of the experiment, but that is -statistically- rather unlikely
- irrespective of group allocation, each participant sees 120 trials in total during the main part (on top of and thus not counting the training trials)
  + every participant sees no item twice (no trial with same `item_name` in master CSV, but items used in training can repeat)
- these 120 trials are presented in 10 blocks of 12 trials each
- the number/kind of trials in each block depends on the group (reliable vs unreliable):
  - reliable group: each block contains 6 reliable critical items and 6 fillers
    - 6 reliable trials   = 3 for each discourse particle
  - unreliable group: each block contains 6 reliable, 4 unreliable critical trial and 2 fillers
    - 6 reliable trials   = 3 for each discourse particle
    - 4 unreliable trials = 2 for each discourse particle


# realization
- R script generates fill list of (already randomized) trials, which will be loaded into magpie
  + the script outputs a number (default 50) of trials sets of the reliable and the unreliable group each
  + the magpie program then only needs to pick a random `item_set_ID` (ranging from 11 to 110) to assign randomly to each participant
  + the first 11-60 `item_set_ID`-s are for the reliable group 
- this is implemented with three scripts:
  1. `01_training_generator.R` provides helper function to generate 10 training trials
  2. `02_trial-generator.R`    provides helper function to generate 120 main trials (reliable or unreliable) 
  3. `03_generate_CSVs.R`      creates variable number of sets of experimental items (training + main; relia. + unrel.) 

