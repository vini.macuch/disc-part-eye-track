library(tidyverse)

# Generate training data 

# running number of participant
# item_set_ID = "11"

# "reliable" vs "unreliable" group
# group_type = "reliable"

generate_trial_CSV = function(item_set_ID, group_type, write_to_file = T) {

  # check input for adequacy
  if (! group_type %in% c("reliable", "unreliable")) {
    stop("Argument `group_type` must be one of `reliable` or unreliable`")
  }
  if (! is.character(item_set_ID)) {
    stop("Argument `item_set_ID` must be a string")
  }
  if (nchar(item_set_ID) < 2) {
    stop("Argument `item_set_ID` must not be a single digit, but should have a leading 0, like in `11`")
  }
  if (as.double(item_set_ID) <= 10) {
    stop("Particpant numbering must start at 11")
  }

  # create training data for the participant ID
  d_training <- generate_training_CSV(item_set_ID, "reliable", F)
  
  # Load item inventory from master CSV file
  # assuming that the cwd is the root of the repo
  d <- read_csv("materials/disc-part-items.csv", col_types = cols())   

  # Add some variables specific to the mousetracking experiment in OpSe
  d$MT_target <- d$picture_target
  d$MT_pic_left <- d$picture_target
  d$MT_pic_right <- d$picture_competitor

  # number of blocks
  n_blocks = 10

  # determine how many trials of each type to extract for each block
  # this depends on the group type ("reliable" vs "unreliable")
  # NB: all numbers supplied here must be multiples of `n_blocks`!
  if (group_type == "reliable") {
    n_reliable_actually   = 3 * n_blocks
    n_reliable_indeed     = 3 * n_blocks
    n_unreliable_actually = 0 * n_blocks
    n_unreliable_indeed   = 0 * n_blocks
    n_fillers             = 6 * n_blocks
  } else {
    n_reliable_actually   = 3 * n_blocks
    n_reliable_indeed     = 3 * n_blocks
    n_unreliable_actually = 2 * n_blocks
    n_unreliable_indeed   = 2 * n_blocks
    n_fillers             = 2 * n_blocks
  }

  # draw `n` trials of condition `condition` and with DP `DP`
  # w/o replacement from master CSV
  # avoid any item supplied in `ignore_items`
  plug_trials = function(DP_select, condition_select, n_samples, ignore_items = "") {
    # show(ignore_items)
    d %>%
      filter(
        DP == DP_select,
        condition == condition_select,
        ! item_name %in% ignore_items
      ) %>%
      sample_n(n_samples, replace = F) %>%
      # add a block number 1, 2, ..., n_blocks
      mutate(block = rep(1:n_blocks, each = n_samples / n_blocks))
    # show(str_c("intersection: ", intersect(out$item_name, ignore_items)))
  }

  # NB: items for reliable and unreliable trials are disjoint already in master CSV
  reliable_actually   = plug_trials("actually", "reliable",   n_reliable_actually)
  reliable_indeed     = plug_trials("indeed",   "reliable",   n_reliable_indeed,    reliable_actually$item_name)
  unreliable_actually = plug_trials("actually", "unreliable", n_unreliable_actually)
  unreliable_indeed   = plug_trials("indeed",   "unreliable", n_unreliable_indeed,  unreliable_actually$item_name)

  # select `n_fillers` filler trials at random w/o replacement
  fillers = d %>%
    filter(item_type == "filler") %>%
    sample_n(n_fillers, replace = F) %>%
    # add a list number 1, 2, ..., n_blocks
    mutate(block = rep(1:n_blocks, each = n_fillers / n_blocks))

  # join all tibbles in a list
  selected_trials <-  list(
    reliable_actually,
    reliable_indeed,
    unreliable_actually,
    unreliable_indeed,
    fillers
  ) %>%
    # discard empty tibbles
    discard(function(x) {nrow(x) == 0}) %>%
    # reduce list -> append each DF to previous
    reduce(rbind) %>%
    # shuffle the trials in each block (randomly)
    group_by(block) %>%
    mutate(
      trial_nr_in_block = sample(1:12, size = 12, replace = F),
      group_type = group_type
    ) %>%
    # order by block and, within each block, by trial_nr_in_block
    arrange(block, trial_nr_in_block) %>% 
    mutate(
      train_or_main = "main",
      item_set_ID = item_set_ID
      ) %>% 
    # remove fauly item
    filter( item_name != "cigar-pipe")
  
  selected_trials <- rbind(d_training, selected_trials)


  # write if flag is set
  if (write_to_file) {
    write_csv(selected_trials, str_c("experiments/mouse-tracking-magpie/trial-data/",item_set_ID,"_trials.csv"))
  }

  selected_trials

}

# generate_trial_CSV("12", "reliable", T)
